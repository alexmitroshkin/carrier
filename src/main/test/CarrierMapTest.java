import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.carrier.config.AppConfig;
import ru.carrier.mapper.CarrierMapper;
import ru.carrier.models.Carrier;

import java.util.List;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = AppConfig.class)
public class CarrierMapTest {

//    @Autowired
    CarrierMapper carrierMapper;

//    @Test
    public void testMapper(){
        List<Carrier> carrierList = carrierMapper.getCarrierList();
        Assert.assertNotNull(carrierList);
    }
}
