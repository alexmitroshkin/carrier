<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Список Перевозчиков</title>
</head>
<body>
<div>
    <table border="1" id="carrierList">
        <thead>
        <tr>
            <th>Название</th>
            <th>ИНН</th>
            <th>Адрес регистрации</th>
            <th>Страна регистрации</th>
            <th>Система налогооблажения</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${carrierList}" var="carrier">
            <tr>
                <td>${carrier.name}</td>
                <td>${carrier.inn}</td>
                <td>${carrier.addressReg}</td>
                <td>${carrier.countryReg}</td>
                <td>${carrier.taxSystem}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
