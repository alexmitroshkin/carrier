package ru.carrier.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.carrier.mapper.CarrierMapper;

@Controller
@RequestMapping("/carrier")
public class CarrierController {

    private final CarrierMapper carrierMapper;

    public CarrierController(CarrierMapper carrierMapper) {
        this.carrierMapper = carrierMapper;
    }

    @GetMapping("/list")
    public String carrierList(Model model) {
        model.addAttribute("carrierList", carrierMapper.getCarrierList());
        return "carrierList";
    }

}
