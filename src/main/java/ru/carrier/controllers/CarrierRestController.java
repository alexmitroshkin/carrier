package ru.carrier.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.carrier.mapper.CarrierMapper;
import ru.carrier.models.Carrier;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rest/carriers")
public class CarrierRestController {

    private final CarrierMapper carrierMapper;

    public CarrierRestController(CarrierMapper carrierMapper) {
        this.carrierMapper = carrierMapper;
    }

    @GetMapping(path = "list")
    public List<Carrier> carrierList(){
        return carrierMapper.getCarrierList();
    }

//    @PostMapping(path = "create")
    @PutMapping(path = "item")
    public void createCarrier(@RequestBody Carrier carrier){
        carrierMapper.createCarrier(carrier);
    }

    @DeleteMapping(path = "item/{id}")
    public void deleteCarrier(@PathVariable Long id){
        carrierMapper.deleteCarrier(id);
    }

    @PutMapping(path = "item/update")
    public void updateCarrier(@RequestBody Carrier carrier){
        Carrier updCarrier = carrierMapper.getCarrier(carrier.getId());
        if (updCarrier!=null) {
            updCarrier.setName(carrier.getName());
            updCarrier.setAddressReg(carrier.getAddressReg());
            updCarrier.setCountryReg(carrier.getCountryReg());
            updCarrier.setInn(carrier.getInn());
            updCarrier.setTaxSystem(carrier.getTaxSystem());
            carrierMapper.updateCarrier(updCarrier);
        }
    }
}
