package ru.carrier.models;

public class Carrier {
    private Long id;
    private String name;
    private String inn;
    private String addressReg;
    private String countryReg;
    private TaxSystem taxSystem;

    public Carrier() {
    }

    public Long getId() {
        return id;
    }

//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getAddressReg() {
        return addressReg;
    }

    public void setAddressReg(String addressReg) {
        this.addressReg = addressReg;
    }

    public String getCountryReg() {
        return countryReg;
    }

    public void setCountryReg(String countryReg) {
        this.countryReg = countryReg;
    }

    public TaxSystem getTaxSystem() {
        return taxSystem;
    }

    public void setTaxSystem(TaxSystem taxSystem) {
        this.taxSystem = taxSystem;
    }

    @Override
    public String toString() {
        return "Carrier{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inn='" + inn + '\'' +
                ", addressReg='" + addressReg + '\'' +
                ", countryReg='" + countryReg + '\'' +
                ", taxSystem=" + taxSystem +
                '}';
    }
}
