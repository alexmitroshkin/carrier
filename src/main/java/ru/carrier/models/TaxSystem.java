package ru.carrier.models;

public enum  TaxSystem {
    WITHOUT_NDS, NDS_ZERO, NDS_TEN, NDS_TWENTY;
}
