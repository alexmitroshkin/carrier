package ru.carrier.mapper;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import ru.carrier.models.Carrier;

import java.util.List;

@Component
public interface CarrierMapper {
    @Select("SELECT * FROM public.carrier WHERE id = #{id}")
    @Results({
            @Result(property = "name", column = "namecarrier")
    })
    Carrier getCarrier(@Param("id") Long id);

    @Select("SELECT * FROM public.carrier")
    @Results({
            @Result(property = "name", column = "namecarrier")
    })
    List<Carrier> getCarrierList();

    @Insert("INSERT INTO public.carrier (namecarrier, inn, addressreg, countryreg, taxsystem) VALUES (#{name}, #{inn}, #{addressReg}, #{countryReg}, #{taxSystem})")
    Long createCarrier(Carrier carrier);

    @Update("UPDATE public.carrier set namecarrier=#{name}, inn=#{inn}, addressreg=#{addressReg}, countryreg=#{countryReg}, taxsystem=#{taxSystem} WHERE id=#{id}")
    Long updateCarrier(Carrier carrier);

    @Delete("DELETE FROM public.carrier WHERE id = #{id}")
    Long deleteCarrier(Long id);
}
