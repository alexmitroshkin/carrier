CREATE DATABASE carrier
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
	
CREATE TABLE public.carrier
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    namecarrier character varying(255) COLLATE pg_catalog."default",
    inn character varying(12) COLLATE pg_catalog."default",
    "addressReg" character varying(255) COLLATE pg_catalog."default",
    "countryReg" character varying(255) COLLATE pg_catalog."default",
    "taxSystem" character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT carrier_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.carrier
    OWNER to postgres;
	
INSERT INTO public.carrier(namecarrier, inn, "addressReg", "countryReg", "taxSystem") 
VALUES ('Name1', '123456', 'Address1', 'Country1', 'WITHOUT_NDS');
INSERT INTO public.carrier(namecarrier, inn, "addressReg", "countryReg", "taxSystem") 
VALUES ('Name2', '1234567', 'Address2', 'Country2', 'NDS_ZERO');
INSERT INTO public.carrier(namecarrier, inn, "addressReg", "countryReg", "taxSystem") 
VALUES ('Name3', '12345678', 'Address3', 'Country3', 'NDS_TEN');
INSERT INTO public.carrier(namecarrier, inn, "addressReg", "countryReg", "taxSystem") 
VALUES ('Name4', '123456789', 'Address4', 'Country4', 'NDS_TWENTY');
